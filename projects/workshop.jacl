#!../bin/fcgijacl
# -----------------------------------------------------------------------------

constant status_window 0
constant game_title		"Dangar Marine"
constant game_author	"Stuart Allen"
constant game_release    1
constant game_build      1
constant ifid			"JACL-012"

# -----------------------------------------------------------------------------

integer now
integer gmt
string  todays_date

integer temp
integer INDEX

integer client_last_id		0
integer part_last_id		0
integer jobpart_last_id		0
integer job_last_id			0
integer editing
integer errors

constant JOB_VIEW			1
constant JOB_EDIT			2
constant CLIENT_VIEW		3
constant CLIENT_EDIT		4
constant PART_VIEW			5
constant PART_EDIT			6
constant INVOICE			7
constant PREFERENCES		8

string tab_names "Workshop" "Job View" "Job Edit" "Client View" "Client Edit" "Part View" "Part Edit" "Invoice" "Preferences"

# -----------------------------------------------------------------------------
# JOB FIELDS

string job_id_data
string job_description_data
string job_description_error
string job_client_data
string job_client_error
string job_date_requested_data
string job_date_requested_error
string job_date_completed_data
string job_date_completed_error
string job_hours_data
string job_hours_error
string job_notes_data
string job_notes_error
string job_date_paid_data
string job_date_paid_error

string job_search_data
string job_search_error

parameter job_id				job_id_data
parameter job_description		job_description_data
parameter job_client			job_client_data
parameter job_date_requested	job_date_requested_data
parameter job_date_completed	job_date_completed_data
parameter job_hours	    		job_hours_data
parameter job_notes				job_notes_data
parameter job_date_paid			job_date_paid_data

parameter job_search			job_search_data

# -----------------------------------------------------------------------------
# CLIENT FIELDS

string client_id_data
string client_name_data
string client_name_error
string client_email_data
string client_email_error
string client_home_phone_data
string client_home_phone_error
string client_mobile_phone_data
string client_mobile_phone_error
string client_address_data
string client_address_error

string client_search_data
string client_search_error

parameter client_id				client_id_data
parameter client_name			client_name_data
parameter client_email			client_email_data
parameter client_home_phone		client_home_phone_data
parameter client_mobile_phone	client_mobile_phone_data
parameter client_address		client_address_data

parameter client_search			client_search_data

# -----------------------------------------------------------------------------
# PART FIELDS

string part_id_data
string part_description_data
string part_description_error
string part_cost_data
string part_cost_error
string part_price_data
string part_price_error
string part_stock_data
string part_stock_error
string part_units_data
string part_units_error
string part_notes_data
string part_notes_error

string part_units_values "Items" "Metres" "Litres" "Kilograms"

string part_search_data
string part_search_error

parameter part_id			part_id_data
parameter part_description	part_description_data
parameter part_cost			part_cost_data
parameter part_price		part_price_data
parameter part_stock	    part_stock_data
parameter part_units	    part_units_data
parameter part_notes		part_notes_data

parameter part_search		part_search_data

# -----------------------------------------------------------------------------
# JOBPART FIELDS

string jobpart_id_data
string jobpart_number_data
string jobpart_number_error
string jobpart_quantity_data
string jobpart_quantity_error

parameter part_id			part_id_data
parameter jobpart_number	jobpart_number_data
parameter jobpart_quantity	jobpart_quantity_data


# -----------------------------------------------------------------------------
# CLIENTS TABS

grammar delete client $integer >delete_client

{+delete_client
update clients
   if field[0] != $integer
      insert field[0] field[1] field[2] field[3] field[4] field[5] field[6]
   endif
endupdate
write "Record " $integer " deleted.^^"
}

# -----------------------------------------------------------------------------
# THIS IS THE ACTION TO INITIATE THE EDITING OF A CLIENT - TO DISLAY THE
# EDIT FORM WITH THE SELECT CLIENT'S DETAILS PREFILLED

grammar edit client $integer >edit_client

{+edit_client
set editing = true
execute "+skip_to_client_record<$integer"
execute "+edit_client_form"
}

{+skip_to_client_record
iterate clients
   if field[0] = $integer
      return
   endif
enditerate
}

{+edit_client_form
write "<h3>Edit existing client:</h3>^"
write "<form method=~get~>"
execute "+text<Name<client_name<50<field[1]"
execute "+text<Email address<client_email<50<field[2]"
execute "+text<Home phone<client_home_phone<50<field[3]"
execute "+text<Mobile phone<client_mobile_phone<50<field[4]"
execute "+text<Address<client_address<50<field[5]"

write "<div class=~row~>"
write "<div class=~label~>&nbsp;</div>
write "<div class=~input~><input type=~submit~ value=~Save~></div>"
write "</div>
write "<input type=~hidden~ name=~command~ value=~update client " field[0] "~/>"
hidden
write "</form>
}

# -----------------------------------------------------------------------------
# WRITE THE NEW CLIENT INFORMATION TO THE DATABASE

grammar update client $integer >update_client

{+update_client
set editing = true
if errors != 0
   # THERE WERE ERRORS, RETURN TO DISPLAY THE FORM AGAIN
   execute "+edit_client_form"
   return
endif
update clients
   if field[0] = $integer
      insert field[0] client_name_data client_email_data client_home_phone_data client_mobile_phone_data client_address_data
   else
      insert field[0] field[1] field[2] field[3] field[4] field[5]
   endif
endupdate
write "Record " $integer " updated.^^"
}

# -----------------------------------------------------------------------------
# DISPLAY THE SEARCH BOX FOR THE CLIENT TABLE FOLLOWED BY THE SEARCH RESULTS

{+client_view
write "<form>"
execute "+text_submit<Search<client_search<60<client_search_data"
write "<input type=~hidden~ name=~command~ value=~search client ~/>"
hidden
write "</form>"
write "^<hr>"
execute "+client_search_results"
}

# -----------------------------------------------------------------------------
# IF THE EDIT TAB IS VISITED WITHOUT A CLIENT RECORD HAVING BEEN SELECTED TO
# EDIT, THIS FORM WILL BE DISPLAYED TO ADD A NEW CLIENT

{+add_client_form
write "<h3>Add new client:</h3>^"
write "<form method=~get~>"
execute "+text<Name<client_name<30"
execute "+text<Email address<client_email<50"
execute "+text<Home phone<client_home_phone<50"
execute "+text<Mobile phone<client_mobile_phone<50"
execute "+text<Address<client_address<50"
write "<div class=~row~>"
write "<div class=~label~>&nbsp;</div>
write "<div class=~input~><input type=~submit~ value=~Add new~></div>"
write "</div>
write "<input type=~hidden~ name=~command~ value=~add client~/>"
hidden
write "</form>
}

grammar add client >add_client_record

# BEFORE PERFORMING THE add_client_record ACTION, VALIDATE THE FIELDS

{+before_add_client_record : +before_update_client
if +validate_email<client_email_data == false
   setstring client_email_error "Invalid e-mail address."
   set errors + 1
endif 
return false
}

{+add_client_record
if errors != 0
   # THERE WERE ERRORS, RETURN TO DISPLAY THE FORM AGAIN WITH THE ERROR MESSAGES
   return
endif
set client_last_id + 1
append clients client_last_id client_name_data client_email_data client_home_phone_data client_mobile_phone_data client_address_data
write client_name_data " added to database.^^"
}

integer client_matches
integer part_matches

string action

grammar search client >client_search

{+client_search
set WEBAPP_mode = CLIENT_VIEW
}

{+client_search_results
set WEBAPP_mode = CLIENT_VIEW
set client_matches = 0
write "<table width=~100%~ class=~customer~>"
write "<tr>"
write "<th>ID</th>"
write "<th>Name</th>"
write "<th>Email address</th>"
write "<th>Home phone</th>"
write "<th>Mobile phone</th>"
write "<th>Address</th>"
write "<th>ACTION</th>"
write "</tr>"

iterate clients
   ifstring field[1] contains client_search_data : field[2] contains client_search_data : field[3] contains client_search_data : field[4] contains client_search_data : field[5] contains client_search_data : field[6] contains client_search_data
      write "<tr>"
      write "<td>" field[0] "</td>"
      write "<td>" field[1] "</td>"
      write "<td>" field[2] "</td>"
      write "<td>" field[3] "</td>"
      write "<td>" field[4] "</td>"
      write "<td>" field[5] "</td>"
      write "<td>"
      setstring action "delete client " field[0]
      hyperlinkNE "Delete" action
      write "<br>"
      setstring action "edit client " field[0] "&mode=" CLIENT_EDIT
      hyperlinkNE "Edit" action
      write "</td>"
      write "</tr>"
      set client_matches + 1 
   endif
enditerate
write "</table>"

if client_matches = 0
   write "^No matching records found.^"
endif
if client_matches = 1
   write "^1 matching record found.^"
endif
if client_matches > 1
   write "^" client_matches " matching records found.^"
endif
}

# -----------------------------------------------------------------------------
# PARTS TABS

grammar delete part $integer >delete_part

{+delete_part
update parts
   if field[0] != $integer
      insert field[0] field[1] field[2] field[3] field[4] field[5] field[6]
   endif
endupdate
}

# -----------------------------------------------------------------------------
# THIS IS THE ACTION TO INITIATE THE EDITING OF A PART - TO DISLAY THE
# EDIT FORM WITH THE SELECT PART'S DETAILS PREFILLED

grammar edit part $integer >edit_part

{+edit_part
set editing = true
execute "+skip_to_part_record<$integer"
execute "+edit_part_form"
}

{+skip_to_part_record
iterate parts
   if field[0] = $integer
      return
   endif
enditerate
}

{+edit_part_form
write "<h3>Edit existing part:</h3>^"
write "<form method=~get~>"
execute "+text<Desciption<part_description<50<field[1]"
execute "+text<Cost<part_cost<10<field[2]"
execute "+text<Price<part_price<10<field[3]"
execute "+text<Stock<part_stock<10<field[4]"
execute "+select<Units<part_units<50<field[5]"
execute "+textarea<Notes<part_notes<80<4<field[6]"

write "<div class=~row~>"
write "<div class=~label~>&nbsp;</div>
write "<div class=~input~><input type=~submit~ value=~Save~></div>"
write "</div>
write "<input type=~hidden~ name=~command~ value=~update part " field[0] "~/>"
hidden
write "</form>
}

# -----------------------------------------------------------------------------
# WRITE THE NEW PART INFORMATION TO THE DATABASE

grammar update part $integer >update_part

{+update_part
set editing = true
if errors != 0
   # THERE WERE ERRORS, RETURN TO DISPLAY THE FORM AGAIN
   execute "+edit_part_form"
   return
endif
update parts
   if field[0] = $integer
      insert field[0] part_description_data part_cost_data part_price_data part_stock_data part_units_data part_notes_data
   else
      insert field[0] field[1] field[2] field[3] field[4] field[5] field[6]
   endif
endupdate
set WEBAPP_mode = PART_VIEW
#write "Record " $integer " updated.^^"
}

# -----------------------------------------------------------------------------
# DISPLAY THE SEARCH BOX FOR THE PART TABLE FOLLOWED BY THE SEARCH RESULTS

{+part_view
write "<form>"
execute "+text_submit<Search<part_search<60<part_search_data"
write "<input type=~hidden~ name=~command~ value=~search part ~/>"
hidden
write "</form>"
write "^<hr>"
execute "+part_search_results"
}

{+part_search_results
set WEBAPP_mode = PART_VIEW
set part_matches = 0
write "<table width=~100%~ class=~customer~>"
write "<tr>"
write "<th>ID</th>"
write "<th>Description</th>"
write "<th>Cost</th>"
write "<th>Price</th>"
write "<th>Stock</th>"
write "<th>Units</th>"
write "<th>Notes</th>"
write "<th>ACTION</th>"
write "</tr>"

iterate parts
   ifstring field[1] contains part_search_data : field[2] contains part_search_data : field[3] contains part_search_data : field[4] contains part_search_data : field[5] contains part_search_data : field[6] contains part_search_data
      write "<tr>"
      write "<td>" field[0] "</td>"
      write "<td>" field[1] "</td>"
      write "<td>" field[2] "</td>"
      write "<td>" field[3] "</td>"
      write "<td>" field[4] "</td>"
      write "<td>" field[5] "</td>"
      write "<td>" field[6] "</td>"
      write "<td>"
      setstring action "delete part " field[0]
      hyperlinkNE "Delete" action
      write "<br>"
      setstring action "edit part " field[0] "&mode=" PART_EDIT
      hyperlinkNE "Edit" action
      write "</td>"
      write "</tr>"
      set part_matches + 1 
   endif
enditerate
write "</table>"

if part_matches = 0
   write "^No matching records found.^"
endif
if part_matches = 1
   write "^1 matching record found.^"
endif
if part_matches > 1
   write "^" part_matches " matching records found.^"
endif
}

# -----------------------------------------------------------------------------
# IF THE EDIT TAB IS VISITED WITHOUT A PART RECORD HAVING BEEN SELECTED TO
# EDIT, THIS FORM WILL BE DISPLAYED TO ADD A NEW PART

{+add_part_form
write "<h3>Add new part:</h3>^"
write "<form method=~get~>"
execute "+text<Desciption<part_description<50"
execute "+text<Cost<part_cost<10"
execute "+text<Price<part_price<10"
execute "+text<Stock<part_stock<10"
execute "+select<Units<part_units<50"
execute "+textarea<Notes<part_notes<80<4"
write "<div class=~row~>"
write "<div class=~label~>&nbsp;</div>
write "<div class=~input~><input type=~submit~ value=~Add new~></div>"
write "</div>
write "<input type=~hidden~ name=~command~ value=~add part~/>"
hidden
write "</form>
}

grammar add part >add_part_record

# BEFORE PERFORMING THE add_part_record ACTION, VALIDATE THE FIELDS

{+before_add_part_record : +before_update_part
if +validate_number<part_stock_data<0<1000 == false
   setstring part_stock_error "Not a valid number."
   set errors + 1
endif 
return false
}

{+add_part_record
if errors != 0
   # THERE WERE ERRORS, RETURN TO DISPLAY THE FORM AGAIN WITH THE ERROR MESSAGES
   return
endif
set part_last_id + 1
append parts part_last_id part_name_description part_cost_data part_price_data part_stock_data part_units_data part_notes_data
write part_description_data " added to database.^^"
}

integer part_matches

string action

grammar search part >part_search

{+part_search
set WEBAPP_mode = PART_VIEW
}

# -----------------------------------------------------------------------------
# JOB TABS

grammar delete job $integer >delete_job

{+delete_job
update jobs
   if field[0] != $integer
      insert field[0] field[1] field[2] field[3] field[4] field[5] field[6] field[7]
   endif
endupdate
}

grammar delete part from job $integer >delete_job_part

{+delete_job_part
update jobparts
   if field[0] != $integer
      insert field[0] field[1] field[2] field[3]
   else 
      setstring jobpart_number_data field[2]
      setstring jobpart_quantity_data field[3]
   endif
endupdate

# UPDATE THE INVENTORY TO REFLECT THE USED PARTS
set INDEX = jobpart_quantity_data
execute "+increase_stock<jobpart_number_data<INDEX"

# NOW GO BACK TO DISPLAYING THE CURRENT JOB BEING EDITING IN CASE IT NEEDS MORE PARTS
set editing = true
execute "+skip_to_job_record<$integer"
execute "+populate_job_fields"
set WEBAPP_mode = JOB_EDIT
}

# -----------------------------------------------------------------------------
# THIS IS THE ACTION TO INITIATE THE EDITING OF A JOB - TO DISLAY THE
# EDIT FORM WITH THE SELECT JOBS'S DETAILS PREFILLED

grammar edit job $integer >edit_job

{+edit_job
set editing = true
set WEBAPP_mode = JOB_EDIT
execute "+skip_to_job_record<$integer"
execute "+populate_job_fields"
}

{+skip_to_job_record
iterate jobs
   if field[0] = $integer
      return
   endif
enditerate
}

{+populate_job_fields
set JOB_INDEX = field[0]
setstring job_description_data field[1]
setstring job_client_data field[2]
setstring job_date_requested_data field[3]
setstring job_date_completed_data field[4]
setstring job_hours_data field[5]
setstring job_notes_data field[6]
setstring job_date_paid_data field[7]
}

integer JOB_INDEX
integer CLIENT_INDEX
integer JOBPART_INDEX

{+edit_job_form
write "<h3>Edit existing job:</h3>^"
write "<form method=~get~>"
execute "+textarea<Desciption<job_description<80<4<job_description_data"
set CLIENT_INDEX = job_client_data
execute "+lookup_select<Client<job_client<clients<1<0<CLIENT_INDEX"
execute "+datepicker<Date Requested<job_date_requested<job_date_requested_data"
execute "+datepicker<Date Completed<job_date_completed<job_date_completed_data"
execute "+text<Hours<job_hours<10<job_hours_data"
execute "+textarea<Notes<job_notes<80<4<job_notes_data"
execute "+datepicker<Date Paid<job_date_paid<job_date_paid_data"

write "<div class=~row~>"
write "<div class=~label~>&nbsp;</div>
write "<div class=~input~><input type=~submit~ value=~Save~></div>"
write "</div>
write "<input type=~hidden~ name=~command~ value=~update job " JOB_INDEX "~/>"
hidden
write "</form>

# -----------------------------------------------------------------------------
# LIST THE EXISTING PARTS USED BY THIS JOB
write "<h3>Billable parts used by this job:</h3>"
write "<table width=~60%~ class=~customer~>"
write "<tr>"
write "<th>Description</th>"
write "<th>Quantity</th>"
write "<th>ACTION</th>"
write "</tr>"

iterate jobparts
   if field[1] = JOB_INDEX
      set JOBPART_INDEX = field[2]
      setstring jobpart_quantity_data field[3]
      write "<tr>"
      write "<td>" JOBPART_INDEX{part_description} "</td>"
      write "<td>" jobpart_quantity_data " " JOBPART_INDEX{part_units} "</td>
      write "<td>
      setstring action "delete part from job " field[0] "&mode=" PART_EDIT
      hyperlinkNE "Delete" action
      write "</td>"
      write "</tr>"
   endif
enditerate
write "</table>"

# -----------------------------------------------------------------------------
# ADD MORE PARTS TO THIS JOB

write "<h3>Add a part to the job:</h3>^"
write "<form method=~get~>"
execute "+lookup_select<Part<jobpart_number<parts<1<0"
execute "+text<Quantity<jobpart_quantity<10"
write "<div class=~row~>"
write "<div class=~label~>&nbsp;</div>
write "<div class=~input~><input type=~submit~ value=~Add~></div>"
write "</div>
write "<input type=~hidden~ name=~command~ value=~add part to job " JOB_INDEX "~/>"
hidden
write "</form>
}

# -----------------------------------------------------------------------------
# WRITE THE NEW JOB INFORMATION TO THE DATABASE

grammar update job $integer >update_job

{+update_job
set editing = true
if errors != 0
   # THERE WERE ERRORS, RETURN TO DISPLAY THE FORM AGAIN
   execute "+edit_job_form"
   return
endif
update jobs
   if field[0] = $integer
      insert field[0] job_description_data job_client_data job_date_requested_data job_date_completed_data job_hours_data job_notes_data job_date_paid_data
   else
      insert field[0] field[1] field[2] field[3] field[4] field[5] field[6] field[7]
   endif
endupdate
set WEBAPP_mode = JOB_EDIT
set editing = true
#write "Record " $integer " updated.^^"
}

# -----------------------------------------------------------------------------
# ADD A NEW PART TO THE JOB THAT IS CURRENTLY BEING EDITED

grammar add part to job $integer >add_part_to_job

# BEFORE PERFORMING THE add_part_record ACTION, VALIDATE THE FIELDS

{+before_add_part_to_job
if +validate_number<jobpart_quantity_data<0<1000 == false
   setstring jobpart_quantity_error "Not a valid quantity."
   set errors + 1
   write "Can't add part to job #" $integer[0] ", invalid quantity ~" jobpart_quantity_data "~.^"
   set editing = true
   execute "+skip_to_job_record<$integer[0]"
   execute "+populate_job_fields"
   execute "+edit_job_form"
   return true
endif 
if +validate_number<jobpart_number_data<0<1000 == false
   setstring jobpart_number_error "Not a valid part ID."
   write "Can't add part to job #" $integer[0] ", invalid part ID ~" jobpart_number_data "~.^"
   set errors + 1
   set editing = true
   execute "+skip_to_job_record<$integer[0]"
   execute "+populate_job_fields"
   execute "+edit_job_form"
   return true
endif 
return false
}

{+add_part_to_job
if errors != 0
   # THERE WERE ERRORS, RETURN TO DISPLAY THE FORM AGAIN WITH THE ERROR MESSAGES
   return
endif
set jobpart_last_id + 1
append jobparts jobpart_last_id $integer[0] jobpart_number_data jobpart_quantity_data

# UPDATE THE INVENTORY TO REFLECT THE USED PARTS
set INDEX = jobpart_quantity_data
execute "+reduce_stock<jobpart_number_data<INDEX"

# NOW GO BACK TO DISPLAYING THE CURRENT JOB BEING EDITING IN CASE IT NEEDS MORE PARTS
set editing = true
execute "+skip_to_job_record<$integer[0]"
execute "+populate_job_fields"
}

integer STOCK_QUANTITY

{+reduce_stock
# arg[0] part number
# arg[1] quantity

update parts
   if field[0] = $integer
      set STOCK_QUANTITY = field[4]
      set STOCK_QUANTITY - arg[1]
      insert field[0] field[1] field[2] field[3] STOCK_QUANTITY field[5] field[6]
   else
      insert field[0] field[1] field[2] field[3] field[4] field[5] field[6]
   endif
endupdate
}

{+increase_stock
# arg[0] part number
# arg[1] quantity

update parts
   if field[0] = $integer
      set STOCK_QUANTITY = field[4]
      set STOCK_QUANTITY + arg[1]
      insert field[0] field[1] field[2] field[3] STOCK_QUANTITY field[5] field[6]
   else
      insert field[0] field[1] field[2] field[3] field[4] field[5] field[6]
   endif
endupdate
}

# -----------------------------------------------------------------------------
# DISPLAY THE SEARCH BOX FOR THE JOB TABLE FOLLOWED BY THE SEARCH RESULTS

{+job_view
write "<form>"
execute "+text_submit<Search<job_search<60<job_search_data"
write "<input type=~hidden~ name=~command~ value=~search job ~/>"
hidden
write "</form>"
write "^<hr>"
execute "+job_search_results"
}

{+job_search_results
set WEBAPP_mode = JOB_VIEW
set job_matches = 0
write "<table width=~100%~ class=~customer~>"
write "<tr>"
write "<th>ID</th>"
write "<th>Description</th>"
write "<th>Client</th>"
write "<th>Date Requested</th>"
write "<th>Date Completed</th>"
write "<th>Hours</th>"
write "<th>Notes</th>"
write "<th>Paid</th>"
write "<th>ACTION</th>"
write "</tr>"

iterate jobs
   ifstring field[1] contains job_search_data : field[2] contains job_search_data : field[3] contains job_search_data : field[4] contains job_search_data : field[5] contains job_search_data : field[6] contains job_search_data : field[7] contains job_search_data
      write "<tr>"
      write "<td>" field[0] "</td>"
      write "<td>" field[1] "</td>"

      # CLIENT FIELD
      write "<td>" field[2]{client_name} "</td>"

      write "<td>" field[3] "</td>"
      write "<td>" field[4] "</td>"
      write "<td>" field[5] "</td>"
      write "<td>" field[6] "</td>"
      write "<td>" field[7] "</td>"
      write "<td>"
      setstring action "delete job " field[0]
      hyperlinkNE "Delete" action
      write "<br>"
      setstring action "edit job " field[0] "&mode=" JOB_EDIT
      hyperlinkNE "Edit" action
      write "<br>"
      setstring action "invoice job " field[0] "&mode=" INVOICE
      hyperlinkNE "Invoice" action
      write "</td>"
      write "</tr>"
      set job_matches + 1 
   endif
enditerate
write "</table>"

if job_matches = 0
   write "^No matching records found.^"
endif
if job_matches = 1
   write "^1 matching record found.^"
endif
if job_matches > 1
   write "^" job_matches " matching records found.^"
endif
}

# -----------------------------------------------------------------------------
# IF THE EDIT TAB IS VISITED WITHOUT A JOB RECORD HAVING BEEN SELECTED TO
# EDIT, THIS FORM WILL BE DISPLAYED TO ADD A NEW JOB

{+add_job_form
write "<h3>Add new job:</h3>^"
write "<form method=~get~>"
execute "+text<Desciption<job_description<50"
execute "+lookup_select<Client<job_client<clients<1<0"
execute "+datepicker<Date Requested<job_date_requested<todays_date"
execute "+datepicker<Date Completed<job_date_completed"
execute "+text<Hours<job_hours<10"
execute "+textarea<Notes<job_notes<80<4"
execute "+datepicker<Date Paid<job_date_paid"
write "<div class=~row~>"
write "<div class=~label~>&nbsp;</div>
write "<div class=~input~><input type=~submit~ value=~Add new~></div>"
write "</div>
write "<input type=~hidden~ name=~command~ value=~add job~/>"
hidden
write "</form>
}

grammar add job >add_job_record

# BEFORE PERFORMING THE add_job_record ACTION, VALIDATE THE FIELDS

{+before_add_job_record : +before_update_job
if +validate_number<job_hours_data<0<1000 == false
   setstring job_hours_error "Not a valid number."
   set errors + 1
endif 
return false
}

{+add_job_record
if errors != 0
   # THERE WERE ERRORS, RETURN TO DISPLAY THE FORM AGAIN WITH THE ERROR MESSAGES
   return
endif
set job_last_id + 1
append jobs job_last_id job_description_data job_client_data job_date_requested_data job_date_completed_data job_hours_data job_notes_data job_date_paid_data
write "Job for " job_client_data " added to database.^^"
set editing = true
execute "+skip_to_job_record<job_last_id"
execute "+populate_job_fields"
execute "+edit_job_form"
}

integer job_matches

string action

grammar search job >job_search

{+job_search
set WEBAPP_mode = JOB_VIEW
}

# -----------------------------------------------------------------------------
# GLOBAL FUNCTIONS

{+after
if WEBAPP_mode = CLIENT_VIEW
   execute "+client_view"
endif
if WEBAPP_mode = PART_VIEW
   execute "+part_view"
endif
if WEBAPP_mode = JOB_VIEW
   execute "+job_view"
endif
ifall WEBAPP_mode = JOB_EDIT : editing = false
   execute "+add_job_form"
endif
ifall WEBAPP_mode = JOB_EDIT : editing = true
   execute "+edit_job_form"
endif
ifall WEBAPP_mode = PART_EDIT : editing = false
   execute "+add_part_form"
endif
ifall WEBAPP_mode = CLIENT_EDIT : editing = false
   execute "+add_client_form"
endif
set editing = false

# CLEAR THE PARAMETERS

setstring jobpart_id_data			""
setstring jobpart_number_data		""
setstring jobpart_number_error		""
setstring jobpart_quantity_data		""
setstring jobpart_quantity_error	""
setstring part_id_data				""
setstring part_description_data		""
setstring part_description_error	""
setstring part_cost_data			""
setstring part_cost_error			""
setstring part_price_data			""
setstring part_price_error			""
setstring part_stock_data			""
setstring part_stock_error			""
setstring part_units_data			""
setstring part_units_error			""
setstring part_notes_data			""
setstring part_notes_error			""

setstring job_id_data				""
setstring job_description_data		""
setstring job_client_data			""
setstring job_date_requested_data	""
setstring job_date_completed_data	""
setstring job_hours_data			""
setstring job_notes_data			""
setstring job_date_paid_data		""
setstring job_description_error		""
setstring job_client_error			""
setstring job_date_requested_error	""
setstring job_date_completed_error	""
setstring job_hours_error			""
setstring job_notes_error			""
setstring job_date_paid_error		""

setstring client_id_data			""
setstring client_name_data    		""
setstring client_name_error			""
setstring client_email_data        	""
setstring client_email_error		""
setstring client_address_data      	""
setstring client_address_error		""
setstring client_home_phone_data   	""
setstring client_home_phone_error	""
setstring client_mobile_phone_data	""
setstring client_mobile_phone_error	""
setstring client_search_data       	""
setstring client_search_error		""

set errors = 0
}

{+links_div
write "Workshop Management Database"
}

{+intro
# GENERATE AND STORE TODAY'S DATE AS A STRING
set gmt = unixtime
set now = +adjusted_time<gmt
setstring todays_date now{integer_date}

set player = you
# GET THE LAST ID
iterate clients
   if field[0] > client_last_id
      set client_last_id = field[0]
   endif
enditerate
iterate parts
   if field[0] > part_last_id
      set part_last_id = field[0]
   endif
enditerate
iterate jobs
   if field[0] > job_last_id
      set job_last_id = field[0]
   endif
enditerate
iterate jobparts
   if field[0] > jobpart_last_id
      set jobpart_last_id = field[0]
   endif
enditerate
execute "+job_view"
}

location boardroom

object you

constant title_image "/images/outboard.jpg"
constant footer_image		"none"
constant header_colour		"#000000"
constant header_height		120
constant linkbar_colour		"#444444"
constant maintext_colour	"#dddddd"
constant tab_colour			"#60594e"

{+local_styles
print:
  #widget-docs .ui-widget input, #widget-docs .ui-widget select, #widget-docs .ui-widget textarea, #widget-docs .ui-widget button { font-size: 10pt; } ^
  #demo-link { font-size:11px;  padding-top: 6px; clear: both; overflow: hidden; }
.
}

{+macro_part_description
lookup return_value arg[0] parts 1
}

{+macro_part_units
lookup return_value arg[0] parts 5
}

{+macro_client_name
lookup return_value arg[0] clients 1
}

#include "forms.library"
#include "time.library"
#include "webapp.library"
#include "webapp.css"
#include "validation.library"
